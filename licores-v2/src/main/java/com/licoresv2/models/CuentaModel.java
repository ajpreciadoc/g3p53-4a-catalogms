package com.licoresv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection="cuentas")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class CuentaModel {
	@Id
    private String username;

}
