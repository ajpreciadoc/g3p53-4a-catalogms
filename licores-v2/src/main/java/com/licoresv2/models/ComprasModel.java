package com.licoresv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection="compras")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ComprasModel {
	@Id
    private String id;
    private String usernameComprador;
    private Integer cantidadProductos;
    private Integer idProducto;


}
