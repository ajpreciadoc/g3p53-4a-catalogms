package com.licoresv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection="catalogoLicores")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class CatalogoLicoresModel {
	@Id
	private Integer id;
	private String tipoLicor;
	private String nombreLicor;
	private Double precio;
	private String presentacionLicor;

}
