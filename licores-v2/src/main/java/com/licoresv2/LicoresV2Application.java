package com.licoresv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LicoresV2Application {

	public static void main(String[] args) {
		SpringApplication.run(LicoresV2Application.class, args);
	}

}
