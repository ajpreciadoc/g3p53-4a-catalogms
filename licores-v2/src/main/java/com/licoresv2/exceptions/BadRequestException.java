package com.licoresv2.exceptions;

public class BadRequestException extends RuntimeException{
    /**
	 * 
	 */
	private static final long serialVersionUID = -6670849918413659661L;

	public BadRequestException(String message){
        super(message);
    }
}

