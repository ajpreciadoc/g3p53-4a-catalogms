package com.licoresv2.exceptions;

public class CuentaNotFoundException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2537748206100060294L;

	public CuentaNotFoundException(String message){
        super(message);
    }
}
