package com.licoresv2.exceptions;

public class LicorNotFoundException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4880404248260179477L;

	public LicorNotFoundException(String message){
        super(message);
    }
}
