package com.licoresv2.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.licoresv2.models.ComprasModel;

@Repository
public interface ComprasRepository extends MongoRepository<ComprasModel,String> {
	
	@Query("{usernameComprador:'?0'}")
	List<ComprasModel> userComprador(String userName);

}
