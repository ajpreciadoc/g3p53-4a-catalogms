package com.licoresv2.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.licoresv2.models.CuentaModel;

@Repository
public interface CuentaRepository extends MongoRepository<CuentaModel,String> {
	
}



