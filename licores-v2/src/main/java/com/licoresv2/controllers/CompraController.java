package com.licoresv2.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.licoresv2.models.ComprasModel;
import com.licoresv2.services.ComprasService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/compras")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE })
@Slf4j

public class CompraController {

	@Autowired
	ComprasService comprasService;

	@GetMapping("/{username}")
	public List<ComprasModel> userCompras(@PathVariable String username) {

		return comprasService.comprasPorUsuario(username);
	}

	@PostMapping("/")
	public ComprasModel newCompra(@RequestBody ComprasModel compra) {
		log.info("compra: " + compra);
		return comprasService.crearCompra(compra);

	}

}
