package com.licoresv2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.licoresv2.exceptions.BadRequestException;
import com.licoresv2.models.CatalogoLicoresModel;
import com.licoresv2.services.CatalogoLicoresService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("/api/licores")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE })
@Slf4j

public class CatalogoLicoresController {

	@Autowired
	CatalogoLicoresService catalogoLicoresService;

	@GetMapping("/{id}")
	public ResponseEntity<CatalogoLicoresModel> getLicor(@PathVariable Integer id) {

		if (id == null) {
			new BadRequestException("Por favor ingrese id");
		}

		CatalogoLicoresModel licor = catalogoLicoresService.licorPorId(id);
		log.info("licor: " + licor);
		return ResponseEntity.ok(licor);

	}

	@PostMapping("/")
	CatalogoLicoresModel newCatalogoLicoresModel(@RequestBody CatalogoLicoresModel catalogoLicoresModel) {
		return catalogoLicoresService.guardarLicor(catalogoLicoresModel);
	}

}
