package com.licoresv2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.licoresv2.models.CuentaModel;
import com.licoresv2.services.CuentaService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/cuenta")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE })
@Slf4j

public class CuentaController {
	
	@Autowired
	CuentaService cuentaService;
	
	@GetMapping("/{username}")
    CuentaModel getCuenta (@PathVariable String username){
		return cuentaService.buscarCuenta(username);                
    }
	
	@PostMapping("/")
    CuentaModel newCuenta (@RequestBody CuentaModel cuenta){
		log.info("cuenta: "+cuenta);
        return cuentaService.crearCuenta(cuenta);
    }

}
