package com.licoresv2.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.licoresv2.exceptions.CuentaNotFoundException;
import com.licoresv2.models.CuentaModel;
import com.licoresv2.repositories.CuentaRepository;

@Service
public class CuentaService {
	
	@Autowired
	CuentaRepository cuentaRepository;
	
	public CuentaModel buscarCuenta (String username){
        return cuentaRepository.findById(username)
                .orElseThrow(() -> new CuentaNotFoundException("no se encontro una cuenta con el username: " + username));
    }
	
	public CuentaModel crearCuenta (CuentaModel cuentaModel){
        return cuentaRepository.save(cuentaModel);
    }

}
