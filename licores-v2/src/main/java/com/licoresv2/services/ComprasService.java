package com.licoresv2.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.licoresv2.exceptions.CuentaNotFoundException;
import com.licoresv2.exceptions.LicorNotFoundException;
import com.licoresv2.models.CatalogoLicoresModel;
import com.licoresv2.models.ComprasModel;
import com.licoresv2.models.CuentaModel;
import com.licoresv2.repositories.CatalogoLicoresRepository;
import com.licoresv2.repositories.ComprasRepository;
import com.licoresv2.repositories.CuentaRepository;

@Service
public class ComprasService {

	@Autowired
	ComprasRepository comprasRepository;

	@Autowired
	CuentaRepository cuentaRepository;

	@Autowired
	CatalogoLicoresRepository catalogoLicoresRepository;

	public List<ComprasModel> comprasPorUsuario(String username) {
		CuentaModel userCuenta = cuentaRepository.findById(username).orElse(null);
		if (userCuenta == null)
			throw new CuentaNotFoundException("no se encontro una cuenta con el username: " + username);

		return comprasRepository.userComprador(username);

	}

	public ComprasModel crearCompra(ComprasModel comprasModel) {
		CuentaModel cuentaCompras = cuentaRepository.findById(comprasModel.getUsernameComprador()).orElse(null);
		if (cuentaCompras == null)
			throw new CuentaNotFoundException(
					"No se encontro una cuenta con el username:" + comprasModel.getUsernameComprador());

		CatalogoLicoresModel productoCompras = catalogoLicoresRepository.findById(comprasModel.getIdProducto())
				.orElse(null);
		if (productoCompras == null)
			throw new LicorNotFoundException("No se encontro un licor con el id:" + comprasModel.getIdProducto());

		comprasModel.setCantidadProductos(comprasModel.getCantidadProductos() + 1);
		return comprasRepository.save(comprasModel);

	}

}
