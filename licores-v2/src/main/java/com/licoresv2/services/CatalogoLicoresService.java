package com.licoresv2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.licoresv2.exceptions.LicorNotFoundException;
import com.licoresv2.models.CatalogoLicoresModel;
import com.licoresv2.repositories.CatalogoLicoresRepository;

@Service
public class CatalogoLicoresService {
	@Autowired
	CatalogoLicoresRepository catalogoLicoresRepository;

	public CatalogoLicoresModel licorPorId(int idLicor) {

		return catalogoLicoresRepository.findById(idLicor)
				.orElseThrow(() -> new LicorNotFoundException("No se encontró un licor con el id: " + idLicor));
	}

	public CatalogoLicoresModel guardarLicor(CatalogoLicoresModel licor) {
		return catalogoLicoresRepository.insert(licor);

	}

}
